export const environment = {
  test: true,
  BASE_URL_STATIC: 'http://localhost:3000',
  DISPATCHER_URL: 'http://localhost:3000',
  ASSET_TRANSLATIONS_PATH: './assets/i18n/bmw',
  ASSET_IMAGES_PATH: './assets/img/bmw/',
  ASSET_STYLES_PATH: './assets/styles/bmw',
  COUNTRY_CODE: 'BE',
};
