export const environment = {
  acceptance: true,
  BASE_URL_STATIC: 'http://localhost:3000',
  DISPATCHER_URL: 'http://localhost:3000',
  ASSET_TRANSLATIONS_PATH: './assets/i18n/mini',
  ASSET_IMAGES_PATH: './assets/img/mini/',
  ASSET_STYLES_PATH: './assets/styles/mini',
  COUNTRY_CODE: 'BE',
};
