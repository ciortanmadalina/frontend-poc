export const environment = {
  test: true,
  production: false,
  acceptance: false,
  BASE_URL_STATIC: '',
  DISPATCHER_URL: '',
  ASSET_TRANSLATIONS_PATH: '',
  ASSET_IMAGES_PATH: '',
  ASSET_STYLES_PATH: '',
  COUNTRY_CODE: 'BE',

};
