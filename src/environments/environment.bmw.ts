// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  BASE_URL_STATIC: 'http://localhost:3000',
  DISPATCHER_URL: 'http://localhost:3000',
  ASSET_TRANSLATIONS_PATH: './assets/i18n/bmw',
  ASSET_IMAGES_PATH: './assets/img/bmw/',
  ASSET_STYLES_PATH: './assets/styles/bmw',
  COUNTRY_CODE: 'BE',
};
