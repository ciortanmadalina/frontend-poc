import { Component, OnInit } from '@angular/core';
import { Angulartics2 } from 'angulartics2';
@Component({
  selector: 'alv-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  constructor(private angulartics2: Angulartics2) { }

  ngOnInit() {
  }

  analytics(index: number){
    this.angulartics2.eventTrack.next({ action: 'Action' + index, properties: { category: 'Category' + index, label: 'Label' + index  }});
  }
}
