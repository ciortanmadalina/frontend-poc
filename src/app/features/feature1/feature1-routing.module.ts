import { CacheComponent } from './components/cache/cache.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { OverviewComponent } from './components/overview/overview.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BackendComponent } from './components/backend/backend.component';
import { TranslationComponent } from './components/translation/translation.component';
import { CodeQualityComponent } from './components/code-quality/code-quality.component';
import { ErrorHandlingComponent } from './components/error-handling/error-handling.component';
@NgModule({
  imports: [
    RouterModule.forChild([
     {path:'backend', component: BackendComponent},
     {path:'translation', component: TranslationComponent},
     {path:'code-quality', component: CodeQualityComponent},
     {path:'error-handling', component: ErrorHandlingComponent},
     {path:'overview', component: OverviewComponent},
     {path:'analytics', component: AnalyticsComponent},
     {path:'cache', component: CacheComponent}
    ])
  ],
  exports : [ RouterModule ]
})
export class Feature1RoutingModule { }
