import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Feature1RoutingModule } from './feature1-routing.module';
import { BackendComponent } from './components/backend/backend.component';
import { TranslationComponent } from './components/translation/translation.component';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from './../../shared/shared.module';
import { BackendService } from './services/backend.service';
import { CodeQualityComponent } from './components/code-quality/code-quality.component';
import { ErrorHandlingComponent } from './components/error-handling/error-handling.component';
import { OverviewComponent } from './components/overview/overview.component';
import { AnalyticsComponent } from './components/analytics/analytics.component';
import { CacheComponent } from './components/cache/cache.component';
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    Feature1RoutingModule,
    SharedModule
  ],
  declarations: [BackendComponent, TranslationComponent, CodeQualityComponent, ErrorHandlingComponent, OverviewComponent, AnalyticsComponent, CacheComponent],
  exports: [BackendComponent, TranslationComponent],
  providers:[BackendService]
})
export class Feature1Module { }
