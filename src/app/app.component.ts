import { environment } from './../environments/environment';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from 'ng2-translate/ng2-translate';
import {Angulartics2GoogleAnalytics} from 'angulartics2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
  TranslateService
]
})
export class AppComponent implements OnInit{
  title = '2speed frontend concepts';
  env = environment;
  constructor(private translateService: TranslateService, 
  angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics) {
}


ngOnInit(): any {
  console.log('hello app component');
        // this language will be used as a fallback when a translation isn't found in the current language
      this.translateService.setDefaultLang('fr');
       // the lang to use, if the lang isn't available, it will use the current loader to get them
      this.translateService.use('fr');
  }


}
