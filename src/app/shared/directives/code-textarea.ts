import { Directive, ElementRef, Renderer, AfterContentChecked, OnInit, NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Directive({
  selector: '[codeTextarea]'
})
export class CodeTextarea implements OnInit, AfterContentChecked {
  @Input('height')
  height:number;
  constructor(private element: ElementRef, private renderer: Renderer) {}

  public ngOnInit(): void {
  }

  ngAfterContentChecked(): void{
    this.adjust();
  }
  adjust(): void{
    this.element.nativeElement.style.overflow = 'hidden';
    this.element.nativeElement.style.height = 'auto';
    this.element.nativeElement.style.height = (this.height) + "px";
    this.element.nativeElement.style.margin = "20px";
    this.element.nativeElement.style.width = "100%";
  }
}
